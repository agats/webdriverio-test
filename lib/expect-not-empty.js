var expect = require( 'chai' ).expect;

function expectNotEmpty ( value ) {
    expect( value ).to.be.not.empty();
}

module.exports = expectNotEmpty;
