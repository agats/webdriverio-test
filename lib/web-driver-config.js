
// WebDriverIO Options
var driverDefaults = {
    host: 'local.rei.com',
    port: 4444,
    logLevel: 'silent',
    waitforTimeout: 99999999,
    desiredCapabilities: {
        browserName: 'phantomjs'
    },
    targetDomain: 'http://local.rei.com:8080'
};

module.exports = driverDefaults;
