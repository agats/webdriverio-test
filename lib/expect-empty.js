var expect = require( 'chai' ).expect;

function expectEmpty ( value ) {
    expect( value ).to.be.empty();
}

module.exports = expectEmpty;
