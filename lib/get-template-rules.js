/**
 * Get Template Rules
 *
 */
var fs =                require( 'fs' );
var path =              require( 'path' );
var TEMPLATE_DIR =      path.resolve( __dirname, '..', 'templates' );
var TEMPLATE_FILES =    fs.readdirSync( TEMPLATE_DIR );
var modules =           [];

modules = TEMPLATE_FILES.map( function ( filename ) {
   return path.resolve( TEMPLATE_DIR, filename );
});

/**
 * Get Template Rules
 * @returns [Array] template rule objects
 */
var getTemplateRules = function ( ) {
    var templates = [];

    modules.forEach( function ( module ) {
        templates.push( require( module ) );
    });

    return templates;
};

module.exports = getTemplateRules;
