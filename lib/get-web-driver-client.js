var webdriverio =       require( 'webdriverio' );
var config =            require( './web-driver-config' );
var _assign =           require( 'lodash/object/assign' );
var _forEach =          require( 'lodash/collection/forEach' );

/**
 * Get Web Driver Client
 * @param {object} options Object of options that can be passed to `webdriverio#remote`
 */
var getWebDriverClient = function ( options ) {
    var client = webdriverio.remote( _assign( config, options ) );

    // Add command to get the Page Meta Data Object for validation
    client.addCommand( 'getPageMetaData', function ( url, callback ) {
        var selfClient = this;

        selfClient
            .url( config.targetDomain + url, function ( ) {
                selfClient.pause( 3000, function ( ) {
                    selfClient.execute( function ( ) {
                        return JSON.stringify( window.utag_data );
                    }, null, function ( err, ret ) {
                        callback( err, JSON.parse( ret.value ) );
                    });
                });
            });
    });

    client.addCommand( 'validateTags', function ( url, validTags, callback ) {
        console.log( 'validateTags' );
        client.getPageMetaData( url, function ( err, data ) {
            console.log( data );
            _forEach( validTags, function ( value, tag ) {
                console.log( value );
                it( tag, function ( ) {
                    expect( value ).to.equal( data[ tag ] );
                });
            });
            callback( err, data );
        });

    });

    return client.init();
};

module.exports = getWebDriverClient;
