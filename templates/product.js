/**
 * TODO: Update url to use a page that is guaranteed to exist. See what we are using in the regression test.
 */

var expect =            require( 'chai' ).expect;
var expectNotEmpty =    require( '../lib/expect-not-empty' );

var template = {
    name:   'Product Page',
    url:    '/product/242098/wigwam-gobi-liners',
    tags: {
        pageName:               'rei:product details:Wigwam Gobi Liners_242098',
        templateType:           'prod_page',
        channel:                'rei_commerce',
        channelAvail:           'both',
        contentType:            'commerce',
        siteId:                 'rei',
        products:               '242098',
        averageRating:          expectNotEmpty,
        reviewCount:            expectNotEmpty
    }
};

template.tags.events = function ( value ) {
    var eventsList = value.split(',');
    expect( eventsList ).to.include( 'prodView' );
    expect( eventsList ).to.include( 'event3' );
    expect( eventsList ).to.include( 'event11' );
};

template.tags.productCategoryPath = function ( value ) {
    var categories = value.split( '|' );
    expect( categories.length ).to.be.at.least( 2 );
};

module.exports = template;
