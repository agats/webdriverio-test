var expect =            require( 'chai' ).expect;
var _ =                 require( 'lodash' );
var expectEmpty =       require( '../lib/expect-empty' );
var expectNotEmpty =    require( '../lib/expect-not-empty' );

var template = {
    name: 'User Search Page',
    url: '/search?query=tents',
    tags: {
        pageName:                   'rei:user_search:tents',
        siteId:                     'rei',
        templateType:               'user_search',
        userSearchTerms:            'tents',
        categoryCanonicalType:      'query',
        categoryCanonicalValue:     'tents',
        channel:                    'rei_commerce',
        contentType:                'commerce',
        currentPagination:          1,
        itemsPerPage:               30,
        sortBy:                     'default',
        lastUserRefinementName:     expectEmpty,
        lastUserRefinementValue:    expectEmpty,
        totalSearchResults:         expectNotEmpty
    }
};

template.tags.events = function ( value, data ) {
    var eventsList = value.split( ',' );

    expect( eventsList ).to.include( 'event23' );

    expect( _.find( eventsList, function ( event ) {
        return event === 'event6=' + data.totalSearchResults;
    })).to.match( /^event6=/ );
};

module.exports = template;
