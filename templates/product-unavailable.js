var expect          = require( 'chai' ).expect;
var expectNotEmpty  = require( '../lib/expect-not-empty' );

var template = {
    name:   'Product Unavailable Page',
    url:    '/product/767241/rei-camp-dome-4-tent',
    tags: {
        pageName:               'error:product unavailable:REI Camp Dome 4 Tent_767241',
        templateType:           'error',
        channel:                'error',
        channelAvail:           'none',
        contentType:            'commerce',
        productCategoryPath:    'Products|Camping and Hiking|Tents|Tent Accessories|Tent Stakes',
        siteId:                 'rei',
        products:               '767241',
        averageRating:          expectNotEmpty
    }
};

template.tags.events = function ( value ) {
    var eventsList = value.split(',');
    expect( eventsList ).to.include( 'event19' );
    expect( eventsList ).to.include( 'event25' );
    expect( eventsList ).to.include( 'event35' );
};

template.tags.productCategoryPath = function ( value ) {
    var categories = value.split( '|' );
    expect( categories.length ).to.be.at.least( 2 );
};

module.exports = template;
