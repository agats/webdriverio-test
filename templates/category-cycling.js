var expect =            require( 'chai' ).expect;
var expectEmpty =       require( '../lib/expect-empty' );
var expectNotEmpty =    require( '../lib/expect-not-empty' );

var template = {
    name: 'Cycling Category Page',
    url: '/h/cycling',
    tags: {
        pageName:                   'rei:cat_main:cycling',
        siteId:                     'rei',
        templateType:               'cat_main',
        channel:                    'rei_commerce',
        contentType:                'commerce'
    }
};

module.exports = template;
