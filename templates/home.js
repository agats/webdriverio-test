var template = {
    name: 'Home Page',
    url: '/',
    tags: {
       pageName:        'rei:home',
       subSection1:     'home',
       subSection2:     'home',
       templateType:    'homepage',
       channel:         'rei_commerce',
       contentType:     'commerce',
       siteId:          'rei'
    }
};

module.exports = template;
