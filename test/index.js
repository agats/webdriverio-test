var webdriverio =       require( 'webdriverio' );
var expect =            require( 'chai' ).expect;
var path =              require( 'path' );
var _ =                 require( 'lodash' );
var templates =         require( '../lib/get-template-rules' )();
var getWebDriver =      require( '../lib/get-web-driver-client' );
var webDriverConfig =   require( '../lib/web-driver-config' );

// Browsers to test against
// TODO parameterize so we can switch between browsers
// var BROWSERS = [ 'firefox', 'chrome'];
var BROWSERS = [ webDriverConfig.desiredCapabilities.browserName ];

// Domain to test against
// TODO parameterize so we can switch between environments
var DOMAIN = webDriverConfig.targetDomain;

BROWSERS.forEach( function ( browserName , index ) {
    var browser = browserName.charAt( 0 ).toUpperCase() + browserName.slice( 1 );

    describe( 'Analytics Data Layer on ' + browser, function () {
        var client = null;

        // Mocha timeout
        this.timeout( 9999999 );

        before( function ( done ) {
            client = getWebDriver();

            // Start up browser on the homepage.
            client.url( DOMAIN, done );
        });

        after( function ( done ) {

            // End connection to remote browser
            client.end( done );
        });

        // Test all templates and tags
        templates.forEach( function ( template, index ) {

            describe( template.name + ' Tags', function () {
                var pageMetaData = null;

                before( function ( done ) {

                    client.getPageMetaData( template.url, function( err, data ) {
                        pageMetaData = data;
                        done();
                    });
                });

                it('has page meta data for analytics', function ( ) {
                    expect( pageMetaData ).to.not.be.empty();
                });

                // Iterate over templates' tags
                _.forEach( template.tags, function ( value, tag ) {

                    it( tag, function () {
                        var pageValue = pageMetaData[ tag ];

                        // Tag value can be a function with its' own specs
                        if ( _.isFunction( value ) ) {
                            value.apply( this, [ pageValue, pageMetaData ] );
                        } else {
                            expect( pageValue ).to.equal( value );
                        }
                    });
                });
            });
        });
    });
});
