var expect =        require( 'chai' ).expect;
var getWebDriver =  require( '../lib/get-web-driver-client' );
var _forEach =      require( 'lodash/collection/forEach' );

describe( 'Home Page', function ( ) {
    var client = getWebDriver();
    var correctData = {
       pageName:        'rei:home',
       subSection1:     'home',
       subSection2:     'home',
       templateType:    'homepage',
       channel:         'rei_commerce',
       contentType:     'commerce',
       siteId:          'rei'
    };

    this.timeout( 999999 );

    // before( function ( done ) {
    //     client.validateTags( '/', correctData, done );
    // });

    after( function ( done ) {
        client.end( done );
    });

    before( function ( done ) {
        client.getPageMetaData( '/', function( err, data ) {
            pageMetaData = data;
            expect( pageMetaData ).to.not.be.empty();
            done();
        });
    });

    _forEach( correctData, function ( value, tag ) {
        it( tag, function ( ) {
            expect( value ).to.equal( pageMetaData[ tag ] );
        });
    });

});
